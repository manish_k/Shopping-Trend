import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { BodyComponent } from './home/body/body.component';
import { ProductDetailComponent } from './home/product-detail/product-detail.component';
import { ProductService } from './home/product.service';
import { FilterPipe } from './home/filter.pipe';
import { HomeViewComponent } from './home/home-view/home-view.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    BodyComponent,
    ProductDetailComponent,
    FilterPipe,
    HomeViewComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      { 
        path:'',
        component: HomeViewComponent, 
      },
      {
        path: 'products/all',
        component: BodyComponent,
      },
      {
        path:'products/grosories',
        component: BodyComponent,
      },
      {
        path: 'products/mobiles',
        component: BodyComponent,
      },
      {
        path: 'products/entertainments',
        component: BodyComponent,
      },
      { 
        path: 'products/:productId', 
        component: ProductDetailComponent 
      }
    ]),
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
