import { Component, OnInit } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

import { ProductService } from '../product.service';
import { Item } from '../shared-data/data.model';




@Component({
  selector: 'app-body',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
     filterItem: string = '';
      products: Item[];
      totalProducts: Item[] = this.productService.getMobileDetails()
                                .concat(this.productService.getGrosoryDetails())
                                .concat(this.productService.getEntertainmentDetails());
      loc: string;
      constructor(private productService: ProductService, private location: Location) {}

      getProductDetails(): void{
          this.loc = this.location.path()
          console.log(this.loc)
          if( this.loc == '/products/all'){
              this.products = this.totalProducts;
          }
          else if (this.loc == '/products/grosories'){
            this.products = this.productService.getGrosoryDetails();
          }
          else if (this.loc == '/products/mobiles') {
          this.products = this.productService.getMobileDetails();
          } else {
            this.products = this.productService.getEntertainmentDetails()
          }
          
      }

      
      ngOnInit() {
        this.getProductDetails()
      }

}
