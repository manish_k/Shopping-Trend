import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterItem : string): any {
   if((value.length === 0) || (filterItem === '')){
     return value
   }
   const resultArray = [];
   for(const item of value){
      if((item.productBrandName.toLowerCase().search(filterItem.toLocaleLowerCase()) === 0)
        ||
        (item.productTitle.toLowerCase().search(filterItem.toLocaleLowerCase()) === 0)
        ||
        (item.productDescription.toLowerCase().search(filterItem.toLocaleLowerCase()) === 0)
      ){
          resultArray.push(item);
      }
      console.log(item)
   }
  return resultArray;
  }

}
