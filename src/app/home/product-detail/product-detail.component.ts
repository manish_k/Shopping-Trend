import { Component, OnInit } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Item } from '../shared-data/data.model';
import { Mobiles } from '../shared-data/mobile.model';
import { Grosories } from '../shared-data/grosories.model';
import { Entertainments } from '../shared-data/entertainment.model';



@Component({
  selector: 'app-product-detail',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  mobiles: Item[] = Mobiles.concat(Grosories).concat(Entertainments);
  id: string = "";
  item:Item;
  test:number;
  i:number;
  constructor(private location: Location) { 
    
  }
  searchItem(path:number):Item {
    for (this.i=0;this.i < this.mobiles.length; this.i++) {
       
       if(this.mobiles[this.i].productId === path){
          this.item = this.mobiles[this.i];
          console.log(this.item)
          return this.item
        }
      }
      return this.item = this.mobiles[0]
  }
  ngOnInit() {
    this.id = this.location.path().split('/')[2].trim();
    this.searchItem(parseInt(this.id))
    this.test = parseInt(this.id)
    console.log(this.test)
    
  }

}
