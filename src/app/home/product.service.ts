import { Injectable } from '@angular/core';



import { Item } from './shared-data/data.model';
import { Grosories } from './shared-data/grosories.model';
import { Mobiles } from './shared-data/mobile.model';
import { Entertainments } from './shared-data/entertainment.model';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  getGrosoryDetails(): Item[]{
    return Grosories;
  }

  getMobileDetails(): Item[]{
    return Mobiles
  }

  getEntertainmentDetails(): Item[]{
    return Entertainments
  }
}
