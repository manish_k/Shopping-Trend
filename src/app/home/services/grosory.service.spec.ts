import { TestBed, inject } from '@angular/core/testing';

import { GrosoryService } from './grosory.service';

describe('GrosoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GrosoryService]
    });
  });

  it('should be created', inject([GrosoryService], (service: GrosoryService) => {
    expect(service).toBeTruthy();
  }));
});
