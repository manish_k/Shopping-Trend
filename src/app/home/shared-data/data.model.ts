export class Item{
    
    productId: number;
    imagePath: string;
    productTitle: string;
    productDescription: string;
    productAmount: number;
    productBrandName: string;

}
